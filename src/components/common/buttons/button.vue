/*
  Component Name: v-button

  Property Definitions:
  - text:
    The text that the button will display on render.

  - type:
    The type of button to render. Valid values are, "button", "link",
    and "routerLink".
      - button: This is the default "type" value. If no value is defined,
        the button will be rendered as a button element. This type requires
        programmatic wiring to use. Example:

          v-button text="Login" v-on:click.native="doSomething"

          v-on:click.native is important and .native must be used to invoke the
          method "doSomething". The method should be rendered as such
          (the example used assumes a route is being invoked):

            methods: {
              doSomething() {
                this.$router.push('goSomewhere');
              },
            },

      - link: If a "link" type is defined, the button will be rendered with an
        anchor tag and may take an href attribute applied to the element
        directly. If the href is referring to a route, ensure that a hash is
        used to prefix the route name as in href="#/routeDestination".
      - routerLink: If a "routerLink" type is defined, the button will be
        rendered as a vue "router-link" element and afford all the benefits and
        properties of such an element. routerLink types take the "destination"
        property as a String and default to the root "/", and may be overriden
        via this property to route the page as needed.

  - size:
    The size property determines the size of the button through a variety of
    pre-defined values. Current values include [from smallest to largest]:
    micro, tiny, small, medium, large, extra-large. Additional sizes may be
    added or removed. Please see the sass definitions below for an updated list.

  - appearance:
    The appearance property takes any pre-defined color value to define the
    visual appearance of the button. Examples include: primary,
    primary-gradient, secondary, secondary-gradient, etc. Additional values
    allow modifying the button's behavior and appearance in a more striking way
    such as the "close-button" appearance value. See the button sass file for
    further values.

  - destination:
    The destination value is used with the "router-link" type to provide a
    routing destination for the button.
*/

<template >
  <a v-bind:class="classes" v-if="type === 'link'">{{ text }}</a>
  <router-link v-bind:class="classes" v-bind:to="destination" v-else-if="type === 'routerLink'">{{ text }}</router-link>
  <button v-bind:class="classes" v-else>{{ text }}</button>
</template>

<script>
export default {
  name: 'v-button',
  props: {
    text: String,
    type: { type: String, default: 'button' },
    size: { type: String, default: 'large' },
    appearance: { type: String, default: 'primary-gradient' },
    destination: { type: String, default: '/' },
  },
  computed: {
    classes() {
      return ['button', this.size, this.appearance].join(' ');
    },
  },
};
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped lang="scss">
  @import "./src/assets/styles/_partials/_common.scss";

  // ------------
  // Button Mixin
  // ------------
  /*================================================
  =            Button Types and Styling            =
  ================================================*/

  /// Lightness Tolerance:  This is a local variable used only in the buttons mixin.
  /// Value from 0 - 100 which adjusts the tolerance of a button background's
  /// lightness value. Lower values allow the button to switch over to a
  /// white text color less easily.  Higher values reduce the likelihood of a
  /// button's color to switch to white.
  /// @example
  ///   @if (lightness( $color ) > 50 ) {
  ///     color: black;
  ///   } @else {
  ///     color: white;
  ///   }
  $lightness-tolerance: 57;


  /// Button Types:  Standard, Punchout, Punchout-Inverted, and
  /// Punchout-Inverted-Masked. A mixin used to pass in a color
  /// from the primary .button class and evaluate if the colors
  /// from the text should be light or dark to remain legible
  /// on the button's background color.  Also evaluates several
  /// button styles as detailed above.
  /// @example .class-name { @include button-color($palette-primary); }
  /// @arg {string} $color - a color value to be used as the background color of the button.
  @mixin button-color($color) {
    background-color: $color;
    @if (lightness($color) > $lightness-tolerance ) {
      color: $palette-dark;

      &::before,
      &::after { color: $palette-dark; }
    }
    @else {
      color: $palette-light;

      &::before,
      &::after { color: $palette-light; }
    }
    &:hover,
    &.hover {
      background-color: lighten($color, 10%);
      @if (lightness($color) > $lightness-tolerance ) {
        color: $palette-dark;
      }
      @else {
        color: $palette-light;
      }
    }

    &.punchout {
      transition: none;
      border: 1px solid $color; //lighten($color, 20%);
      background-color: transparent;
      color: $color;
      font-weight: 600;

      &:hover,
      &.hover {
        background-color: $color;
        color: $palette-light;
        box-shadow: none;
      }
      &.inverted {
        transition: box-shadow 0.25s ease-in-out;
        border: 1px solid $palette-light;
        background-color: transparent;
        color: $palette-light;

        &:hover,
        &.hover {
          transition: box-shadow 0.25s ease-in-out;
          box-shadow: inset 0 0 0 2px $palette-light;
          }

        &.masked {
          transition: background-color .15s ease-in-out, box-shadow 0.25s ease-in-out;
          background-color: rgba(darken($palette-dark-grey, 18%), .6);

          &:hover,
          &.hover { background-color: rgba(darken($palette-dark-grey, 10%), .75); }
        }
      }
    }
  }

  /// Gradient Buttons:  Works the same as button-color, but takes two arguments for creating
  /// a three-dimensional effect with gradients and drop-shadows. It is possible and preferred
  /// to utilize the same color value but with one being modified via sass color functions such
  /// as lighten, darken, hue, etc.
  /// @example Line Line 1 uses two colors. Line 2 uses one color modified via the sass lighten function.
  ///   .class-name { @include button-gradient($palette-light-grey, $palette-dark-grey); }
  ///   .class-name { @include button-gradient($palette-primary, lighten($palette-primary, 10%)); }
  /// @arg {string} $color-primary - a color value to be used as the gradient start point.
  /// @arg {string} $color-secondary - a color value to be used as the gradient end point.
  @mixin button-gradient($color-primary, $color-secondary) {
    transition: .15s box-shadow ease-in-out;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);
    background: linear-gradient(to bottom, $color-primary 0%, $color-secondary 100%);
    background-color: $color-primary;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.3);

    @if (lightness($color-primary) > $lightness-tolerance ) {
      color: $palette-dark;
    }
    @else {
      color: $palette-light;
    }
    &:hover,
    &.hover {
      background: linear-gradient(to bottom, lighten($color-primary, 10%) 0%, lighten($color-secondary, 10%) 100%);
      background-color: lighten($color-primary, 10%);
      box-shadow: 0 1px 3px rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.15);

      @if (lightness($color-primary) > $lightness-tolerance ) {
        color: $palette-dark;
      }
      @else {
        color: $palette-light;
      }
    }
    &:active,
    &.active { background: linear-gradient(to bottom, $color-secondary 0%, $color-primary 100%); }
  }

  /// Sets the color of the icon used in a button.
  /// Useful for overriding the icon color to something
  /// other than the button text.
  /// @example .class-name { @include icon-color($palette-primary); }
  /// @arg {string} $color - a color value to be used as the color of the icon.
  @mixin icon-color($color) {
    @if (lightness($color) > $lightness-tolerance ) {
      color: $palette-dark;
    }
    @else {
      color: $palette-light;
    }
  }

  // --------------
  // Button Styling
  // --------------
  .button {
    display: inline-block;
    position: relative;
    transition: all .15s ease-in-out;
    border: 1px solid transparent;
    border-radius: $border-radius;
    outline: none;
    color: $palette-light;
    font-family: $font-stack-primary;
    font-size: $type-size-medium;
    font-weight: normal;
    line-height: 1; /* this value is unitless by design.  See http://meyerweb.com/eric/thoughts/2006/02/08/unitless-line-heights/ for more info */
    text-align: center;
    text-decoration: none;
    cursor: pointer;
    vertical-align: middle;
    user-select: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    &.disabled,
    &[disabled],
    &[disabled='disabled'],
    fieldset[disabled] & {
      cursor: not-allowed;
      opacity: 0.5;
      pointer-events: none;
    }

    &:focus,
    &.focus { box-shadow: 0 0 5px rgba(81, 203, 238, 1); }

    /* size */
    &.extra-large {
      @include type-size($type-size-large);
      padding: 14px 45px;
    }
    &.large { padding: 12px 15px; }
    &.medium {
      @include type-size($type-size-medium);
      padding: 6px 12px;
    }
    &.small {
      @include type-size($type-size-small);
      padding: 5px 11px;
    }
    &.tiny {
      @include type-size($type-size-smaller);
      padding: 3px 4px;
    }
    &.micro {
      @include type-size($type-size-smallest);
      padding: 2px 3px;
      text-transform: uppercase;
    }

    /* color */
    &.primary { @include button-color($brand-primary); }
    &.secondary { @include button-color($brand-secondary); }
    &.tertiary { @include button-color($brand-tertiary); }
    &.quaternary { @include button-color($brand-quaternary); }
    &.white-grey { @include button-color($palette-white-grey); }
    &.dark-grey { @include button-color($palette-dark-grey); }

    /* gradient */
    &.primary-gradient { @include button-gradient($brand-primary, saturate((darken($brand-primary, 10%)), 5%)); }
    &.secondary-gradient { @include button-gradient($brand-secondary, saturate((darken($brand-secondary, 10%)), 5%)); }
    &.tertiary-gradient { @include button-gradient($brand-tertiary, saturate((darken($brand-tertiary, 10%)), 5%)); }
    &.quaternary-gradient { @include button-gradient($brand-quaternary, saturate((darken($brand-quaternary, 10%)), 5%)); }
    &.white-grey-gradient { @include button-gradient($palette-white-grey, saturate((darken($palette-white-grey, 10%)), 5%)); }
    &.dark-grey-gradient { @include button-gradient($palette-dark-grey, saturate((darken($palette-dark-grey, 10%)), 5%)); }

    /* Miscellaneous */
    &.facebook-gradient { @include button-gradient(#627eb2, saturate((darken(#627eb2, 10%)), 5%)); }
    &.facebook { @include button-color(#627eb2); }

    /* color any icons the color of the text */
    &.primary::before { @include icon-color($brand-primary); }
    &.secondary::before { @include icon-color($brand-secondary); }
    &.tertiary::before { @include icon-color($brand-tertiary); }
    &.quaternary::before { @include icon-color($brand-quaternary); }

    /* behavior */
    &.fluid { display: block; }
  }

  /* Close Button */
  .button-close {
    position: absolute;
    top: 10px;
    right: 10px;
    text-decoration: none;

    &:hover { text-decoration: none; }
  }
  .button-close::after {
    content: '\d7';
    transition: all .15s ease-in-out;
    color: darken($palette-medium-grey, 10%);
    font-size: 2.2rem;
    line-height: 1rem;
    text-decoration: none;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  .button-close:hover::after { color: darken($palette-medium-grey, 30%); }
  .button-close:active::after { color: lighten($palette-medium-grey, 10%); }
</style>
