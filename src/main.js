// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App';
import Styleguide from './routes/styleguide/styleguide';

const routes = [
  { path: '/', component: App },
  { path: '/styleguide', component: Styleguide },
];

// This makes all the magic hapen and Vue recognizes the router-view and router-link
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
});
